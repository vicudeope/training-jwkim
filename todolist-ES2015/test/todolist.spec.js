import CONST from '../src/js/constant';
import Todo from '../src/js/todo.js';


describe('todolist', function() {
    beforeEach(function() {
        jasmine.clock().install();
        this.todo = defaultSetting();
    });

    afterEach(function() {
        jasmine.clock().uninstall();
    });

    describe('addTodo', function() {

        it('empty testname', function() {
            var result = false;
            expect(function() {
                this.todo.addTodo();
            }.bind(this)).toThrow(new Error('needTodoName'));
        });


        it('Should be added to todoHashMap at normal request.', function() {
            this.todo.addTodo('test1');
            expect(this.todo.todoHashMap.get(this.todo.lastSeq)).not.toBeNull();
        });
        it('name registered in todo should match', function() {
            this.todo.addTodo('test1');
            expect(this.todo.todoHashMap.get(this.todo.lastSeq).todoname).toBe('test1');
        });
        it('time registered in todo should be the current time.', function() {
            this.todo.addTodo('test1');
            expect(this.todo.todoHashMap.get(this.todo.lastSeq).createDt).toBe(new Date().getTime());
        });
        it('default false if it is a normal request.', function() {
            this.todo.addTodo('test1');
            expect(this.todo.todoHashMap.get(this.todo.lastSeq).complete).toBe(false);
        });
        it('todoHashMap must match the number of registered todo.', function() {
            this.todo.addTodo('test1');
            this.todo.addTodo('test2');
            expect(this.todo.todoHashMap.size).toBe(2);
        });
    });

    describe('toggleComplete', function() {
        it('Completion status should be changed.', function() {
            this.todo.addTodo('test1');
            expect(this.todo.todoHashMap.get(this.todo.lastSeq).complete).toBe(false);

            this.todo.todoHashMap.get(this.todo.lastSeq).toggleComplete();
            expect(this.todo.todoHashMap.get(this.todo.lastSeq).complete).toBe(true);
        });
    });

    describe('sorting', function() {
        it('should be sorted by the date of registration.', function() {
            var i = 1;
            for (; i <= 7; i += 1) {
                jasmine.clock().mockDate(new Date(2013, i, 23));
                this.todo.addTodo('test' + i);
            }
            this.todo.updateArray();
            expect(this.todo.todoList[0].createDt).toBeGreaterThan(this.todo.todoList[6].createDt);
            expect(this.todo.todoList[0].createDt).toBeGreaterThan(this.todo.todoList[3].createDt);
        });

        it('The completed list should be placed at the bottom.', function() {
            var i = 1;
            for (; i <= 7; i += 1) {
                jasmine.clock().mockDate(new Date(2013, i, 23));
                this.todo.addTodo('test' + i);
            }

            this.todo.todoHashMap.get(7).toggleComplete();
            this.todo.todoHashMap.get(6).toggleComplete();
            this.todo.todoHashMap.get(5).toggleComplete();

            this.todo.updateArray();

            expect(this.todo.todoList[0].complete).toBe(false);
            expect(this.todo.todoList[6].complete).toBe(true);
        });
    });

    describe('filtering', function() {
        it('If the state of the filter is complete, only the todo of the complete state should be shown.', function() {
            var result = true;

            var i = 1;
            for (; i <= 7; i += 1) {
                jasmine.clock().mockDate(new Date(2013, i, 23));
                this.todo.addTodo('test' + i);
            }

            this.todo.todoHashMap.get(7).toggleComplete();
            this.todo.todoHashMap.get(6).toggleComplete();
            this.todo.todoHashMap.get(5).toggleComplete();

            this.todo.filterType = CONST.FILTER_STATE.COMPLETE;
            this.todo.updateArray();

            this.todo.todoList.forEach(function(todo) {
                if (!todo.complete) {
                    result = false;
                }
            });
            expect(result).toBe(true);
        });

        it('After you delete a done list, you should not see it in the list.', function() {
            var result = true;
            var i = 1;
            for (; i <= 7; i += 1) {
                jasmine.clock().mockDate(new Date(2013, i, 23));
                this.todo.addTodo('test' + i);
            }

            this.todo.todoHashMap.get(7).toggleComplete();
            this.todo.todoHashMap.get(6).toggleComplete();
            this.todo.todoHashMap.get(5).toggleComplete();

            this.todo.removeUncompleteTodo();
            this.todo.updateArray();

            this.todo.todoList.forEach(function(todo) {
                if (todo.complete) {
                    result = false;
                }
            });
            expect(result).toBe(true);
        });
    });

    describe('Dom', function() {
        beforeEach(function() {
            this.todo = defaultSetting();
        });
        describe('Input section', function() {
            it('You should be able to get the HTMLElement via the init option.', function() {
                expect(this.todo.domHandler.input).not.toBeUndefined(true);
                expect(this.todo.domHandler.list).not.toBeUndefined(true);
                expect(this.todo.domHandler.removeTodoButton).not.toBeUndefined(true);
            });
            it('When you enter the content, it is added to todo.', function() {
                this.todo.domHandler.input.value = 'test 111';
                fireKey(this.todo.domHandler.input, 13);

                expect(this.todo.todoHashMap.size).toBe(1);
                expect(this.todo.todoList[0].todoname).toBe('test 111');
            });
            it('need to initialize the input state after it is added to todo.', function() {
                this.todo.domHandler.input.value = 'test 111';
                fireKey(this.todo.domHandler.input, 13);
                expect(this.todo.domHandler.input.value).toBe('');
            });
        });

        describe('List output section.', function() {
            it('Todo li must be added to the ul element.', function() {
                var ul = this.todo.domHandler.list;
                var i = 1;
                for (; i <= 7; i += 1) {
                    this.todo.domHandler.input.value = 'test 111';
                    fireKey(this.todo.domHandler.input, 13);
                }

                expect(ul.querySelectorAll('li').length).toBe(7);
            });

            it('Click the checkbox to change the completion status.', function() {
                var ul = this.todo.domHandler.list;
                var checkbox = null;
                var i = 1;
                var seq = 0;
                for (; i <= 7; i += 1) {
                    this.todo.domHandler.input.value = 'test 111';
                    fireKey(this.todo.domHandler.input, 13);
                }
                checkbox = ul.querySelector('li input[name="seq"]');
                seq = Number(checkbox.value);
                trigger('click', checkbox);

                expect(this.todo.todoHashMap.get(seq).complete).toBe(true);
            });
        });
        describe('Footer info section.', function() {
            it('When you click the Clear completed list button, the action is performed.', function() {
                var ul = this.todo.domHandler.list;
                var checkboxList = null;
                var i = 1;
                for (; i <= 7; i += 1) {
                    this.todo.domHandler.input.value = 'test 111';
                    fireKey(this.todo.domHandler.input, 13);
                }

                checkboxList = this.todo.domHandler.list.querySelectorAll('li input[name="seq"]');
                trigger('click', checkboxList[1]);
                trigger('click', this.todo.domHandler.removeTodoButton);

                expect(ul.querySelectorAll('li').length).toBe(6);
            });

            it('Change the remaining count output each time you press Done.', function() {
                var domHandler = this.todo.domHandler;
                var ul = domHandler.list;
                var checkbox = null;
                var uncompleteCount = 0;
                var completeCount = 0;
                var i = 1;
                for (; i <= 7; i += 1) {
                    this.todo.domHandler.input.value = 'test 111';
                    fireKey(this.todo.domHandler.input, 13);
                }

                checkbox = ul.querySelectorAll('li input[name="seq"]')[0];
                trigger('click', checkbox);

                uncompleteCount = parseInt(domHandler.uncompleteCount.innerHTML, 10);
                completeCount = parseInt(domHandler.completeCount.innerHTML, 10);

                expect(uncompleteCount).toBe(6);
                expect(completeCount).toBe(1);
            });

            it('Todo Provides the ability to filter the list.', function() {
                var domHandler = this.todo.domHandler;
                var ul = domHandler.list;
                var checkbox = null;
                var i = 1;
                for (; i <= 7; i += 1) {
                    domHandler.input.value = 'test 111';
                    fireKey(this.todo.domHandler.input, 13);
                }
                checkbox = ul.querySelectorAll('li input[name="seq"]')[0];

                trigger('click', checkbox);
                trigger('click', this.todo.domHandler.filterButton.querySelector('[data-filtertype="1"]'));

                expect(this.todo.filterType).toEqual(CONST.FILTER_STATE.COMPLETE);
            });
        });
    });
});

function defaultSetting() {
    var todo = new Todo({
        'input': document.createElement('input'),
        'list': document.createElement('ul'),
        'uncompleteCount': document.createElement('span'),
        'completeCount': document.createElement('span'),
        'removeTodoButton': document.createElement('button'),
        'filterButton': document.createElement('div')
    });
    return todo;
}

/**
 * event trigger
 * @param {string} eventName - eventName
 * @param {HTMLElement} elementSeletor - event target domelement
 */
function trigger(eventName, elementSeletor) {
    var evt = document.createEvent('HTMLEvents');
    var el = elementSeletor;
    evt.initEvent(eventName, true, true);
    el.dispatchEvent(evt);
}
/**
 * key event trigger
 * @param {HTMLElement} el - event target domelement
 * @param {number} key - key code
 */
function fireKey(el, key) {
    var eventObj = null;
    if (document.createEventObject) {
        eventObj = document.createEventObject();
        eventObj.keyCode = key;
        el.fireEvent('onkeydown', eventObj);
        eventObj.keyCode = key;
    } else if (document.createEvent) {
        eventObj = document.createEvent('Events');
        eventObj.initEvent('keydown', true, true);
        eventObj.which = key;
        eventObj.keyCode = key;
        el.dispatchEvent(eventObj);
    }
}
